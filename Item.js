class Item {
    constructor(product_code,product_name,price){
        this.product_code = product_code;
        this.product_name = product_name;
        this.price = price;
    }
}
module.exports = Item;
