class PricingRule {
	/* 
	qualifier
		0 = a minimum quantity is required to trigger
		1 = every set will trigger
	free_type
		0 = adjust price
		1 = add an item
 	*/	
	constructor(product_code, quantity, qualifier, free_type, adjust_price, free_item) {
		this.product_code = product_code;
		this.quantity = quantity;
		this.qualifier = qualifier;
		this.free_type = free_type;
		this.adjust_price = adjust_price;
		this.free_item = free_item;
	}
}
module.exports = PricingRule;
