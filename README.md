# Simple Javascript class implementing a Shopping Cart

## Quick Start

node ShoppingCartTest.js  

The above command will test only the 4 Scenarios stated on the specs and other related tests. This has no additional module requirements except assert.  

For expanded scope (e.g. adjust price, add new catalogue item) use the command below:

npm test

You need mocha for this one.

## Prerequisites

bash  
yum install nodejs or apt-get install nodejs  
npm install mocha  

## JSON database
promo.json  - Set all promo codes here  
catalogue.json -  Set all items here  

## Classes
ShoppingCart   
Item  
PricingRule  

## Using npm
npm test  
npm start  

