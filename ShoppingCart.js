var Item = require('./Item');

class ShoppingCart {
	constructor(pricingRules) {
		this.pricingRules = pricingRules;
		this.prTally = {}; // will hold the potential matches to pricingRules 
		for (const [item, pricingRule] of Object.entries(pricingRules)) {
			this.prTally[item] = pricingRule.quantity;
			//console.log("Tally quantity is " + this.prTally[item]);
		}
		this.items = [];
		this.total = 0.00;
		this.catalogue = {};
		var catalogue = require('./catalogue.json');
		for(var promo_code in catalogue){
			this.catalogue[catalogue[promo_code].promo_code] = new Item(catalogue[promo_code].promo_code,catalogue[promo_code].product_name,catalogue[promo_code].price);
		}	
		this.promo_code = {};
		var promo = require('./promo.json');
		for(var promo_code in promo){
			this.promo_code[promo[promo_code].promo_code] = promo[promo_code].discount;
		}	
	}
	
	add(item, promo_code) {
		//console.log("Promo code is " + promo_code);
		this.items.push(this.catalogue[item]);
		if(item in this.pricingRules){
			if(this.pricingRules[item].free_type == 0) { //price-based promos
				this.prTally[item] -= 1;
				//console.log("Current quantity is " + this.prTally[item]);
				if(this.prTally[item] == 0) {
					//console.log("Before promo total is " + this.total);
					if(this.pricingRules[item].qualifier == 0) { // minimum-based
						this.total += ((this.pricingRules[item].adjust_price - this.catalogue[item].price) * (4 - this.prTally[item]));
					}else if(this.pricingRules[item].qualifier == 1) { // set-based
						this.total -= this.catalogue[item].price;
						this.prTally[item] = this.pricingRules[item].quantity;
					}
					//console.log("After promo total is " + this.total);
				}
			}else if(this.pricingRules[item].free_type == 1) { //item-based promos
				//console.log("Item is " + item + " and free item is " + this.pricingRules[item].free_item);
				this.items.push(this.catalogue[this.pricingRules[item].free_item]);
			}
		}
		this.total += this.catalogue[item].price;
		if (promo_code in this.promo_code) { 
			//console.log("We got a promo!");
			this.total *= (1 - this.promo_code[promo_code]/100);
		}
		//console.log("Curent total is " + this.total);
	}
}
module.exports = ShoppingCart;
