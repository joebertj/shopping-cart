var assert = require('assert');
var ShoppingCart = require('./ShoppingCart');
var PricingRule = require('./PricingRule');
// Pricing Rules
var pricingRules = {};
pricingRules['ult_small'] = new PricingRule('ult_small',3,1,0,0); // Every 3 items will grant 1 item free of charge
pricingRules['ult_large'] = new PricingRule('ult_large',4,0,0,39.9); // A minimum of 4 items will reduce the price to 39.9 for each
pricingRules['ult_medium'] = new PricingRule('ult_medium',1,1,1,0.0,'1gb'); // Every purchase will get one new free item`
//assert.equal(true, 'ult_small' in pricingRules);
var cart = new ShoppingCart(pricingRules);
console.log('Testing empty cart');
assert.equal(cart.total, 0);
console.log('Testing cart with one ult_small item');
cart.add('ult_small');
assert.equal(cart.total, 24.90);
console.log('Testing cart with one ult_small and one ult_medium items');
cart.add('ult_medium');
assert.equal(cart.total, 24.90 + 29.90);
console.log('Testing cart with one ult_small, ult_medium and one ult_large items');
cart.add('ult_large');
assert.equal(cart.total, 24.90 + 29.90 + 44.90);
console.log('Testing cart with one ult_small, ult_medium, ult_large and one 1gb items');
cart.add('1gb');
assert.equal(cart.total, 24.90 + 29.90 + 44.90 + 9.90);
// Reset test
cart = new ShoppingCart(pricingRules);
console.log('Testing cart with three ult_small items');
cart.add('ult_small');
cart.add('ult_small');
cart.add('ult_small');
assert.equal(cart.total, 2 * 24.90);
console.log('Testing cart with four ult_small items');
cart.add('ult_small');
assert.equal(cart.total, 3 * 24.90);
// Reset test
cart = new ShoppingCart(pricingRules);
console.log('Testing cart with three ult_small and one ult_large items (Scenario 1)');
cart.add('ult_small');
cart.add('ult_small');
cart.add('ult_small');
cart.add('ult_large');
assert.equal(cart.total.toFixed(2), 94.70);
// Reset test
cart = new ShoppingCart(pricingRules);
console.log('Testing cart with two ult_small and four ult_large items (Scenario 2)');
cart.add('ult_small');
cart.add('ult_small');
cart.add('ult_large');
cart.add('ult_large');
cart.add('ult_large');
cart.add('ult_large');
assert.equal(cart.total, 209.40);
assert.equal(cart.items.length, 6);
// Reset test
cart = new ShoppingCart(pricingRules);
console.log('Testing cart with one ult_small and two ult_medium item (Scenario 3)');
cart.add('ult_small');
cart.add('ult_medium');
cart.add('ult_medium');
assert.equal(cart.total.toFixed(2), 84.70);
console.log('Testing free items (Scenario 3)');
assert.equal(cart.items.length, 5);
assert.equal(cart.items[2].product_code, '1gb');
assert.equal(cart.items[4].product_code, '1gb');
console.log(cart.items);
// Reset test
cart = new ShoppingCart(pricingRules);
console.log('Testing cart with one ult_small and one 1gb items with Promo Code 1<3AMAYSIM (Scenario 4)');
cart.add('ult_small');
cart.add('1gb','I<3AMAYSIM');
assert.equal(cart.total.toFixed(2), 31.32);
console.log('All test passed');
