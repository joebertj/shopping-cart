var assert = require('assert');
var ShoppingCart = require('./ShoppingCart');
var PricingRule = require('./PricingRule');

// Setup Pricing Rules
var pricingRules = {};
pricingRules['ult_small'] = new PricingRule('ult_small',3,1,0,0); // Every 3 items will grant 1 item free of charge
pricingRules['ult_large'] = new PricingRule('ult_large',4,0,0,39.9); // A minimum of 4 items will reduce the price to 39.9 for each
pricingRules['ult_medium'] = new PricingRule('ult_medium',1,1,1,0.0,'1gb'); // Every purchase will get one new free item

describe('Testing empty cart', function () {
	it('Total price of empty cart', function() {
		var cart = new ShoppingCart(pricingRules);
		assert.equal(cart.total, 0);
 	});
});
describe('Testing cart with one ult_small item', function () {
	it('Total price of one ult_small item', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		assert.equal(cart.total, cart.catalogue['ult_small'].price);
 	});
});
describe('Testing cart with one ult_small and one ult_medium items', function () {
	it('Total price of one ult_small and one ult_medium items', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		cart.add('ult_medium');
		assert.equal(cart.total, cart.catalogue['ult_small'].price + cart.catalogue['ult_medium'].price);
 	});
});
describe('Testing cart with one ult_small, one ult_medium and one ult_large items', function () {
	it('Total price of one ult_small, one ult_medium and one ult_large items', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		cart.add('ult_medium');
		cart.add('ult_large');
		assert.equal(cart.total, cart.catalogue['ult_small'].price + cart.catalogue['ult_medium'].price + cart.catalogue['ult_large'].price);
 	});
});
describe('Testing cart with one ult_small, one ult_medium, one ult_large and one 1gb items', function () {
	it('Total price of one ult_small, one ult_medium, one ult_large and one 1gb items', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		cart.add('ult_medium');
		cart.add('ult_large');
		cart.add('1gb');
		assert.equal(cart.total, cart.catalogue['ult_small'].price + cart.catalogue['ult_medium'].price + cart.catalogue['ult_large'].price + cart.catalogue['1gb'].price);
 	});
});
describe('Testing cart with three ult_small items', function() {
	it('Total price of three ult_small items', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		cart.add('ult_small');
		cart.add('ult_small');
		assert.equal(cart.total, 2 * cart.catalogue['ult_small'].price);
 	});
});
describe('Testing cart with four ult_small items', function() {
	it('Total price of four ult_small items', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		cart.add('ult_small');
		cart.add('ult_small');
		cart.add('ult_small');
		assert.equal(cart.total, 3 * cart.catalogue['ult_small'].price);
 	});
});
describe('Testing cart with three ult_small and one ult_large items (Scenario 1)', function() {
	it('Total price of three ult_small and one ult_large items', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		cart.add('ult_small');
		cart.add('ult_small');
		cart.add('ult_large');
		assert.equal(cart.total, 2 * cart.catalogue['ult_small'].price + cart.catalogue['ult_large'].price);
 	});
});
describe('Testing cart with two ult_small and four ult_large items (Scenario 2)', function() {
	it('Total price of two ult_small and four ult_large items', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		cart.add('ult_small');
		cart.add('ult_large');
		cart.add('ult_large');
		cart.add('ult_large');
		cart.add('ult_large');
		assert.equal(cart.total, 209.40);
 	});
});
describe('Testing cart with one ult_small and two ult_medium items (Scenario 3)', function() {
	var cart = new ShoppingCart(pricingRules);
	it('Total price of one ult_small and two ult_medium items', function() {
		cart.add('ult_small');
		cart.add('ult_medium');
		cart.add('ult_medium');
		assert.equal(cart.total, cart.catalogue['ult_small'].price + cart.catalogue['ult_medium'].price + cart.catalogue['ult_medium'].price);
 	});
	it('Testing number of items', function() {
		assert.equal(cart.items.length, 5);
 	});
	it('Testing free item 1', function() {
		assert.equal(cart.items[2].product_code, '1gb');
 	});
	it('Testing free item 2', function() {
		assert.equal(cart.items[4].product_code, '1gb');
 	});
});
describe('Testing cart with one ult_small and one 1gb items with Promo Code 1<3AMAYSIM (Scenario 4)', function() {
	it('Total price of one ult_small and one 1gb items with Promo Code 1<3AMAYSIM', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_small');
		cart.add('1gb','I<3AMAYSIM');
		assert.equal(cart.total, (cart.catalogue['ult_small'].price + cart.catalogue['1gb'].price) * (1 - cart.promo_code['I<3AMAYSIM']/100));
 	});
});
describe('Testing cart with one ult_large item with Promo Code AMAYSIMFTW', function() {
	it('Total price of one ult_large item with Promo Code AMAYSIMFTW', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('ult_large','AMAYSIMFTW');
		assert.equal(cart.total, cart.catalogue['ult_large'].price * (1 - cart.promo_code['AMAYSIMFTW']/100));
 	});
});
describe('Testing cart with one 2gb item', function () {
	it('Total price of one 2gb item', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.add('2gb');
		assert.equal(cart.total, cart.catalogue['2gb'].price);
 	});
});
describe('Testing cart with one ult_small item and price changed to 20.00', function () {
	it('Total price of one ult_small item', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.catalogue['ult_small'].price = 20.00
		cart.add('ult_small');
		assert.equal(cart.total, cart.catalogue['ult_small'].price);
 	});
});
describe('Testing cart with three ult_small items and price changed to 20.00', function() {
	it('Total price of three ult_small items', function() {
		var cart = new ShoppingCart(pricingRules);
		cart.catalogue['ult_small'].price = 20.00
		cart.add('ult_small');
		cart.add('ult_small');
		cart.add('ult_small');
		assert.equal(cart.total, 2 * cart.catalogue['ult_small'].price);
 	});
});
